# Travel

Keeping track of travel suggestions as I wander around!

## Netherlands

### Amsterdam

- https://docs.google.com/document/d/1JHJqyb-YSHtxnqwY1If0OcWNjKFeNSAWpc66kodntIk/edit
- excellent public transit (need pre paid cards)
- Public transport is pretty expensive. If you will use it a lot, a OV chipkaart is worth the money. 
- english widely available
- need chip and pin card
- shelter city near the red light district and dam square
- @Iamsterdam 
- boop @_j3lena_ @wicca_NL @nemesis09 for drinks

#### see
- @rijksmuseum
- @vangoghmuseum
- Zaanse Schans to see windmills from the 1600s
- Van Gogh museum
- Ann Frank's house perhaps (prebook for the last one)
- A canal cruise is a really good way to see the city.
- The Escher Museum in Den Haag (about an hour outside of the city) is my absolute favorite
- Microbial, the world's first and I think only microbial museum

#### eat
- https://t.co/O14K9OahmQ?amp=1
- Proeflokaal Arendsnest beer
- eat stroopwaffles and pancakes
- Golden duck near Kam Kee in Zeedijk
- Eat Oliebollen, Dutch cheese and FEBO Croquettes.
- kebabs

#### Work
- Libraries OBA
- American book center or Pllek

#### Stay
- Flying Pig hostel
- Cocomama hostel

## United States

### Iowa (IA)

#### Des Moines

- Fongs Pizza and have the crab Rangoon Pizza
- Tasty Tacos and have some delicious tacos.
- Ray Gun t-shirt shop, it's amazing
- https://www.eateryadsm.com/ 
- Farmers Market and get some brunch there https://www.dsmpartnership.com/desmoinesfarmersmarket/. The Farmers Market draws a lot of people but is a nice event that is happening while you are here.
Some things to do while you are in town (assuming you will be near downtown):
- East Village https://eastvillagedesmoines.com/ - lots of little shops, bars and unique DSM experiences. Some of my favorite shops in the area are: Ray Gun https://www.raygunsite.com/, Fontenelle Supply Co (men’s clothing store but cool vibe) https://www.fontenellesupplyco.com/, Ephemera https://ephemeradesign.com/, Domestica https://www.domestica.shop/
 - Good eateries and bars in the East Village: Zombie Burger, Alba, The Republic
 - Other downtown options: Centro, Akebono, Malo, Teddy Maroons and the Cheese Bar (a little ways away from downtown but still pretty close)
- If you like coffee, check out Horizon Line coffee - https://www.horizonlinecoffee.com/
- The Sculpture Park https://www.desmoinesartcenter.org/visit/pappajohn-sculpture-park is a cool place to walk around and take in some outdoor art
- The DSM Art Center https://www.desmoinesartcenter.org/ is a little away from downtown but is a cool place to take in some art.
- Gray’s Lake is fun to walk around but has flooded a few times this summer and has some construction. With the projected weather this weekend and back to school happening, it will be really busy.
- If you are looking for a brewery, Confluence or Exile would be my guidance. Exile has food and Confluence normally has food trucks near by. (edited) 
- If you have extra time, you could head to the Art Center or Capital. The Science Center currently has a cool Lego Exhibit as well https://www.sciowa.org/visit/permanent-exhibits/brick-by-brick/. 

### Kansas

#### Pittsburg

- Signet Coffee Roasters

#### Witchita

- https://georgesfrenchbistro.com/

### Nebraska (NE)

- https://www.thunderheadbrewing.com/location/

### Oaklahoma (OK)

#### Tulsa
- [36 Degrees North](https://www.36degreesnorth.co) co-working
- [Andolini's Pizza](https://www.google.com/maps/place/Andolini's+Pizzeria/@36.1404112,-95.9718851,17z/data=!3m1!4b1!4m5!3m4!1s0x87b6ec96e08e570d:0x468b92314f0b0933!8m2!3d36.1404112!4d-95.9696964)
- Prairie Fire Pie (nearby) 
- bbq, check out Burn Co.
- For beer, American Solera's taproom at 18th and Boston - many other breweries are at like 6th and Lewis (not far), but Solera is the most unique/local.
- Glacier Confection, 209 E Archer St, Tulsa, OK 74103
- Foolish Things Coffee Company, 1001 S Main St, Tulsa, OK 74119

### Utah

#### Salt Lake City

- Bjorn's Brew (2165 State Street, South Salt Lake, UT 84115)
- Alpha Coffee (7260 Racquet Club Dr, Cottonwood Heights, UT 84121)
- Normal Ice Cream

### Wisconsin (WI)

#### Madison

- Staying downtown if you have the budget for it because you’ll be able to walk to a lot of things.
- UW campus, one of the student unions (Memorial Union) has a big lakefront terrace that’s nice to sit on and gaze out at the lake; they often have live music too, and you can get beer or ice cream. There’s WiFi. 
- The observation deck in the state Capitol is free if you want a quick nice view of the city. 
- During lunch there are a bunch of food carts in two main spots: the Capitol square and Library Mall, a pedestrian area on the UW campus. Lots of really tasty local options in both spots, everything from Indonesian to Venezuelan to falafel.
- State Street (which runs from the Capitol to Library Mall) has several good local restaurants. 
- The Old Fashioned (also on the square) is yummy and has lots of local beer (it was good!) 
- For brunch downtown, Marigold. 
- Quintessential Wisconsin foods to sample; frozen custard [Michael's Frozen Custard](http://www.ilovemichaels.com/) or ice cream, and cheese curds.

## Canada
