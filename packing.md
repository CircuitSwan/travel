YMMV but here is how I pack

It will vary based on length of trip, destination(s), purpose(s), where I plan to stay, etc.

* TSA compliant sizes moved to TSA liquid bag when doing carryon only

** grab day of

*** checked bag only

## Luggage
- Samsonite expandable soft sided carry-on, black, that i spray painted a stripe on
- hard sided IDK from my mom when i do a checked bag, it's pretty messed up by now

## Toiletries (always packed, dups as needed)
- toiletry bag (had coach-not enough compartments, had ali express - useless compartments, unhappy)
- toothbrush and plastic cover/cap (a second one, lives in my travel bag gets rotated evry 3 months with new)
- flosspicks
- small floss
- hairbands (a few spares)
- plastic alligator hair clip
- hairbrush (a second one, this lives in my travel bag)
- nail clippers
- cuiticle clippers
- tweezers
- nitrile gloves in a plastic bag
- q-tips in travel plastic hard container
- travel pack tissues
- tampon & maxipad (yes 1 of each, i'm ablated i should not need them plus i wear dear kates and thinkx)
- eyemask & earplugs (from a virgin atlantic flight in their carry case)
- *eye moisturizing drops like refresh, individual when i can bottle otherwise
- *sunscreen
- *[nudd deoderant](https://nuudcare.us/)
- *toothpaste

## Electronics
- laptop backpack (The ADV3NTURE 3-in-1 Backpack, good not great)
- Laptop charger (one that lives in my laptop backpack)
- cheap USB wireless mouse runs on 1 AA battery, can lose and not care (one that lives in my laptop backpack)
- wall wort [PowerPort 4 Ports](https://us.anker.com/) (one that lives in my laptop backpack)
- Anker extension cord [PowerPort Strip 3 with 3 USB Ports](https://us.anker.com/) (one that lives in my laptop backpack)
- Laptop stand (roost)
- mousepad
- USB AOC monitor
- 3 zip swag pouch with the following:
  - Power pack(s)
  - Charging cables (phones, power packs, headset) (bag of which that lives in my laptop backpack)
  - cat5
- USB memory stick
- USB condom
- USB adapter
- ** yubikey
- ** Laptop (pack day of)
- ** 2 phones
- ** Bose Quiet Comfort II headset & case w/carabeaner to clip on outside of backpack (pack day of)
- emergency bag - 1 change of clothes in a small zip bag in carry-on

## Security
- [Veritas Traveller's Doorstop Item 05K9920](https://www.leevalley.com/en-us/shop/home/travel/40812-veritas-travellers-doorstop?item=05K9920)
- [security strap](www.supergriplock.com)

## First Aid
- pouch - mollee compatable organe pouch can't find where i got it now!
- adhesive bandages
- antibiotic ointment packets
- antiseptic wipe packets
- aspirin
- tylenol / Acetaminophen
- advil / Ibuprofen
- aleve / Naproxen
- gasx
- immodium
- tums
- azo
- pseudafed
- benedryl
- pepto bismol
- nyquil
- dayquil
- alcaselzer
- emergen-c
- pepcid ac
- 1 emergency blanket
- rain poncho
- non latex gloves
- liquid bandage
- individual packets of burn cream
- antacids (rolaids and generic pills)
- alcohol wipes
- Tweezers
- saftey pins

need:
- something snacky?
- quick clot
- tagaderm
- 1 breathing barrier
- 1 instant cold compress
- hydrocortisone ointment packets 
- gauze roll
- sterile gauze pads
- Oral thermometer
- pulse ox
- scisors

## Medical
- prescription pills in daily pill packs then in ziplock
- **prescription eye drops
- **night mouthguard
- wrist brace
- night wrist brace
- knee brace

## Clothes
- shoes
- bedroom slippers
- flip flops / shower shoes
- socks (smartwool and darntough)
- pants
- underwear (she thinkx, dear kate)
- thigh savers
- bras
- shirts
- dresses
- outerwear
- PJs
- Bathingsuit & coverup
- shawls
- jewlery
- hat

## EDC
- purse
- wallet (credit cards)
- cash & coins
- sunglasses
- *hand sanitizer
- face masks
- keys
- sunglasses
- empty glasses case
- glasses wipes
- *glasses spray
- collapseable grocery sack
- *tide pen
- *chapstick
- *mini superglue
- saftey pins
- dice
- eletrical tape
- flashlight
- pen
- sharpie
- notepad
- ***wine opener (cheap so if stolen by tsa it's ok) [similar to this](https://www.webstaurantstore.com/black-customizable-plastic-pocket-corkscrew/208300801.html?utm_source=google&utm_medium=cpc&utm_campaign=GoogleShopping&gclid=Cj0KCQjw5auGBhDEARIsAFyNm9FMeCpzt-7zgkRLg52TG2gX0EDaIRexaxyvykz9HwC9pdhINPXnRAUaApPjEALw_wcB)
- bottle opener (cheap so if stolen by tsa it's ok)
- mini sewing kit
- mirror
- metal straw
- chopsticks (collapseable)
- little folding knife ***

need:
- duct or gaffers tape

## Identification
- nexus
- passport
- drivers license
- perminant resident card

## paperwork
- printouts as needed with itinerary, addresses etc

## entertainment
- books
- magazines
- switch, games, power, case, assessories

## conference
- ID pin from https://namebadgesinternational.us/
- business cards
- presentation clicker
- video adapter
- stickers

## international
- power adapters

## other
- collapsable duffel bag in case i have swag etc
- puppy pads / waterproof sheet
- condoms
- lube
- flask and funnel
- RFID blocking (wallet, card holder, etc)
- snacks, drinks
- nalgene water bottle
- sometimes makeup and perfume

also see: https://docs.google.com/spreadsheets/d/1rUe9rCtHJR9jM-d3EL-b2ec-QF-fTBMNgSFBZ-CfQOc/edit#gid=0
